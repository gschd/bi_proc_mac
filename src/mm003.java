
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;

public class mm003 extends Frame implements WindowListener, ActionListener {

    static mm003 mm=null;
    Button bt1=null;
    Image img;
    int[] hist;
    boolean hfg = false;
    int Iw, Ih;
    int[][] r,g,b;
    double[][] cy,ci,cq;
    BufferedImage im1=null, im2=null;

    mm003()
    {
        this.setSize(600,600);
        this.setLocation(50,200);
        this.addWindowListener(this);
        this.setVisible(true);
        this.setLayout(null);

        bt1 = new Button("OK");
        bt1.setSize(80,40);
        bt1.setLocation(20,80);
        this.add(bt1);
        bt1.addActionListener(this);

//        String filename = "/Users/iwalis/Desktop/jLabs/src/28774666645_c1b379724d_b.jpg";
        String filename = "/Users/iwalis/Desktop/jLabs/src/L002.jpg";
        img = IP.LoadPicture( filename );

//        String filename = "28774666645_c1b379724d_b.jpg";
//        img = Toolkit.getDefaultToolkit().createImage(
//          this.getClass().getResource(filename)
//        );

    }

    public static void main(String[] a)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new mm003();
            }
        });
    }

    public void paint(Graphics g)
    {

        g.drawImage(img, 100,100, 400,400, this);
        if( hfg == true )
        {
            for(int i=0; i<256; i++)
            {
                g.drawLine( i, 0, i, 300-hist[i] );
            }
        }

    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

//        //im1=(BufferedImage)img;
//        Ih=im1.getHeight(); Iw=im1.getWidth();
//        byte[] data = IP.Image2Data(im1);

        im1 = (BufferedImage)img;
        Ih = im1.getHeight(); Iw = im1.getWidth();
        byte[] data = IP.Image2Data( im1 );

        int pt=0; byte xb;

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);

//	  for(int i=0; i<Ih; i++)
//		  for(int j=0; j<Iw; j++) { cy[i][j]=cy[i][j]*0.5; }
        hist=IP.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);
//        IP.YIQ2RGB( r, g, b, cy, ci, cq, Iw, Ih );

        //** Convert To 1D **
        IP.RGB2ImageData(r, g, b, data, Iw, Ih);

        //****************************
        im1=IP.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();



        //bt1.setLabel("Yes, OK.");

//        im1 = (BufferedImage)img;
//        Ih = im1.getHeight(); Iw = im1.getWidth();
//        byte[] data = IP.Image2Data( im1 );
//
//        int pt=0; byte xb;
//
//        // convert to 2D
//        r = new int[Ih][Iw]; g = new int[Ih][Iw]; b = new int[Ih][Iw];
//        IP.ImageData2RGB( data, r, g, b, Iw, Ih );
//
//
//
//        // main image process R,G,B
//        // RGB -> YIQ
//        cy = new double[Ih][Iw]; ci = new double[Ih][Iw]; cq = new double[Ih][Iw];
//        IP.RGB2YIQ( r, g, b, cy, ci, cq, Iw, Ih );
//
////        for(int i=0; i<Ih; i++)
////            for(int j=0; j<Iw; j++)
////            {
////                cy[i][j] = cy[i][j]*0.8;
////            }
//
//        hist = IP.Histogram( cy, Iw, Ih );
////        hfg = true;
//
//        int aa=0, bb=0, p=(int)((Iw*Ih)*0.1);
//        int sum=0;
//        for(int i=255; i>=0; i--) {
//            sum = sum + hist[i];
//            if( sum>p ) { bb=i; break; }
//        }
//
//        int[] LUT = IP.LUT( aa, bb );
//        for(int i=0; i<Ih; i++)
//            for( int j=0; j<Iw; j++ )
//                cy[i][j] = LUT[ (int)(cy[i][j]) ];
//
//
//
//        IP.YIQ2RGB( r, g, b, cy, ci, cq, Iw, Ih );
//
//
//        // convert to 1D
//        pt = 0;
//        for(int i=0; i<Ih; i++)
//            for(int j=0; j<Iw; j++)
//            {
//                data[pt]=(byte)(r[i][j] & 0xff); pt++;
//                data[pt]=(byte)(g[i][j] & 0xff); pt++;
//                data[pt]=(byte)(b[i][j] & 0xff); pt++;
//            }
//
//
//        img = IP.Data2Image( data, Iw, Ih );
//        repaint();

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        dispose();
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
