import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;


public class IP_sTEA {

    public static void RGB2YIQ(int[][] r, int[][] g, int[][] b,
                               double[][] cy, double[][] ci, double[][] cq, int Iw, int Ih )
    {
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j]=r[i][j]* 0.299+g[i][j]* 0.587+b[i][j]* 0.114;
                ci[i][j]=r[i][j]* 0.596+g[i][j]*-0.275+b[i][j]*-0.321;
                cq[i][j]=r[i][j]* 0.212+g[i][j]*-0.523+b[i][j]* 0.311;
            }
    }

    public static void YIQ2RGB(double[][] cy, double[][] ci, double[][] cq,
                               int[][] r, int[][] g, int[][] b, int Iw, int Ih )
    {	int pt=0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  pt=(int)(cy[i][j]*1+ci[i][j]* 0.956+cq[i][j]* 0.621);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                r[i][j]=pt;
                pt=(int)(cy[i][j]*1+ci[i][j]*-0.272+cq[i][j]*-0.647);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                g[i][j]=pt;
                pt=(int)(cy[i][j]*1+ci[i][j]*-1.105+cq[i][j]* 1.702);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                b[i][j]=pt;
            }
    }

    public static void ImageData2RGB(byte[] data, int[][] r, int[][] g, int[][] b, int Iw, int Ih)
    {
        int pt=0;  byte xb;
        //** Change order of pixels from BGR to RGB **
        while(pt<(Iw*Ih*3)) { xb=data[pt]; data[pt]=data[pt+2]; pt++; pt++; data[pt]=xb; pt++; }

        pt=0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            { r[i][j]=(int)(data[pt] & 0xff); pt++; g[i][j]=(int)(data[pt] & 0xff); pt++;
                b[i][j]=(int)(data[pt] & 0xff); pt++; }
    }

    public static void RGB2ImageData(int[][] r, int[][] g, int[][] b, byte[] data, int Iw, int Ih)
    {
        int pt=0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            { data[pt]=(byte)(r[i][j] & 0xff); pt++; data[pt]=(byte)(g[i][j] & 0xff); pt++;
                data[pt]=(byte)(b[i][j] & 0xff); pt++; }
    }

    public static void RGB2ImageData_RightLeft( BufferedImage Img )
    {
        int w = Img.getWidth();
        int h = Img.getHeight();

        int[][] data = new int[w][h];
        for( int i=0; i<h; i++ ) {
            for( int j=0; j<w; j++ ) {
                data[j][i] = Img.getRGB(j, i);
            }
        }

        /*
        *
                        for (int i = 0; i < h; i++) {
                    for (int j = 0, b = w - 1; j < w; j++, b--) {
                        tmps[b][i] = datas[j][i];
                    }
                }
        *
        */

        int[][] tmp = new int[w][h];
        for( int i=0; i<h; i++ ) {
            for( int j=0, b=w-1; j<w; j++, b-- ) {
                tmp[b][i] = data[j][i];
            }
        }

        for( int i=0; i<h; i++ ) {
            for( int j=0; j<w; j++ ) {
                Img.setRGB( j, i, tmp[j][i] );
            }
        }
    }

    public static void RGB2ImageData_UpDown( BufferedImage Img )
    {
        int w = Img.getWidth();
        int h = Img.getHeight();

        int[][] data = new int[w][h];
        for( int i=0; i<h; i++ ) {
            for( int j=0; j<w; j++ ) {
                data[j][i] = Img.getRGB(j, i);
            }
        }

        int[][] tmp = new int[w][h];
        for( int i=0, a=h-1; i<h; i++, a-- ) {
            for( int j=0; j<w; j++ ) {
                tmp[j][a] = data[j][i];
            }
        }

        for( int i=0; i<h; i++ ) {
            for( int j=0; j<w; j++ ) {
                Img.setRGB( j, i, tmp[j][i] );
            }
        }

    }




    public static BufferedImage rotateImage(final BufferedImage bufferedimage, final int degree)
    {
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(h, w, type)).createGraphics()).setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), w/2, h/2 + (w>h?(w-h):(h-w)/2) );
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }

/*
    public static BufferedImage rotateImage(final BufferedImage bufferedimage,final int degree){
        int w = bufferedimage.getWidth();
        int h = bufferedimage.getHeight();
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(h, w, type)).createGraphics()).setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2 + (w>h?(w-h)/2:(h-w)/2));
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }

*/

        /***

         　　* 圖片旋轉數據處理

         　　* @param datas

         　　* @param width

         　　* @param height

         　　* @param FX 0 為上下反轉 1 為左右反轉

         　　* @return

         　　*/



    /*
　　public static int [][] XZ(int [][] datas,int width,int height,int FX)
　　{
　　  try {

　　      int[][] tmps= new int [width][height];

　　      if(FX==0) {
　　          for(int i = 0,a=height-1;i< height;i ,a--) {
　　              for(int j = 0 ,b=width-1;j<width;j ,b--) {
　　                  tmps[b][a] = datas[j][i];
　　              }
　　          }
　　      }　else if (FX==1) {

　　          for(int i = 0;i< height;i ) {
　　              for(int j = 0 ,b=width-1;j<width;j ,b--) {
                　　tmps[b][i] = datas[j][i];
                }
　　          }
　　      }

　　      return tmps;

　　  } catch (Exception e) {

　　      // TODO: handle exception
　　      return null;
　   }

　　}
*/


    public static BufferedImage LoadPicture(String filename)
    {
        File in=new File(filename); Image img=null;
        try{ img=ImageIO.read(in); } catch(Exception e) {  }
        return(  (BufferedImage)img );
    }

    public static byte[] Image2Data(BufferedImage im1)
    {  return( ((DataBufferByte)im1.getRaster().getDataBuffer()).getData() ); }

    public static BufferedImage Data2Image(byte[] data, int Iw, int Ih)
    {	BufferedImage im1 = new BufferedImage(Iw,Ih, BufferedImage.TYPE_3BYTE_BGR);
        im1.getRaster().setDataElements(0,0,Iw,Ih,data);
        return( im1 );
    }

    public static int[] Histogram(double[][] y, int Iw, int Ih)
    {  int[] histo=new int[256];
        for(int i=0; i<256; i++) histo[i]=0;

        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++) {  histo[ (int)(y[i][j])  ]++; }
        return( histo );
    }

    public static int[] LUT(int a, int b)
    {   int[] p=new int[256];
        for(int i=0; i<a; i++) { p[i]=0; }
        for(int i=255; i>b; i--) { p[i]=255; }
        for(int i=a; i<=b; i++) { p[i]=(int)( (i-a)*(255/(b-a)) ); }

        return( p );
    }

    public static void BufferedImage_toSaveFile( BufferedImage Img )
    {
        try {
            String filename = "/Users/iwalis/Desktop/jLabs_out.png";

            File outputfile = new File( filename );
            ImageIO.write(Img, "png", outputfile);



        } catch (IOException e) {
            // handle exception


        }
    }

    public static void Img2Fuliya( BufferedImage Img )
    {

    }

}



