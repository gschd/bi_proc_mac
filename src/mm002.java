import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;

public class mm002 extends Frame implements WindowListener, ActionListener {
    static mm002 mm=null;
    Button bt1=null;
    Image img;
    int Iw, Ih;
    BufferedImage im1=null, im2=null;

    mm002()
    {
        this.setSize(600,600);
        this.setLocation(50,200);
        this.addWindowListener(this);
        this.setVisible(true);
        this.setLayout(null);

        bt1 = new Button("OK");
        bt1.setSize(80,40);
        bt1.setLocation(20,80);
        this.add(bt1);
        bt1.addActionListener(this);



        String filename = "/Users/iwalis/Desktop/Labs_JAVA/src/28774666645_c1b379724d_b.jpg";
        File in = new File(filename);
        try{ img= ImageIO.read(in); }
        catch(Exception e) { System.out.println( e.toString() ); }



//        String filename = "28774666645_c1b379724d_b.jpg";
//        img = Toolkit.getDefaultToolkit().createImage(
//          this.getClass().getResource(filename)
//        );

    }

    public static void main(String[] a)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new mm002();
            }
        });
    }


    public void paint(Graphics g)
    {
        g.drawImage(img, 100,100, 400,400, this);
    }

        @Override
        public void windowOpened(WindowEvent windowEvent) {

        }

        @Override
        public void windowClosing(WindowEvent windowEvent) {
            dispose();
        }

        @Override
        public void windowClosed(WindowEvent windowEvent) {

        }

        @Override
        public void windowIconified(WindowEvent windowEvent) {

        }

        @Override
        public void windowDeiconified(WindowEvent windowEvent) {

        }

        @Override
        public void windowActivated(WindowEvent windowEvent) {

        }

        @Override
        public void windowDeactivated(WindowEvent windowEvent) {

        }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        //bt1.setLabel("Yes, OK.");

        im1 = (BufferedImage)img;
        Ih = im1.getHeight(); Iw = im1.getWidth();
        byte[] data = ((DataBufferByte)im1.getRaster().getDataBuffer()).getData();

//        im1 = new BufferedImage(Iw, Ih, BufferedImage.TYPE_3BYTE_BGR);
//        im1.getRaster().setDataElements(0,0,Iw,Ih,data);
//        img = (Image)im1;
//        repaint();

        int pt=0; byte b;
        while(pt<(Iw*Ih*3))
        {
            // orig image is RGB; but JAVA is BGR
            b = data[pt];
            data[pt]=data[pt+2];
            pt++; pt++;
            data[pt]=b; pt++;
        }

        pt=0; int bb=0;
        while(pt<(Iw*Ih*3))
        {
            bb=(int)(data[pt] & 0xff);
            bb=bb+(int)(data[pt+1] & 0xff);
            bb=bb+(int)(data[pt+2] & 0xff);
            bb=bb/3;

//            data[pt]=(byte)bb; pt++;
//            data[pt]=(byte)bb; pt++;
//            data[pt]=(byte)(bb*9/10); pt++;

            data[pt]=(byte)(bb*1.2); pt++;
            data[pt]=(byte)(bb*1.2); pt++;
            data[pt]=(byte)(bb*1.2); pt++;

        }




        im1 = new BufferedImage(Iw, Ih, BufferedImage.TYPE_3BYTE_BGR);
        im1.getRaster().setDataElements(0,0,Iw,Ih,data);
        img=(Image)im1;
        repaint();

    }
}
